<?php

namespace Drupal\drupal_up\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class drupalUpController
 * @package Drupal\drupal_up\Controller
 */
class drupalUpController extends  ControllerBase
{
    /**
     * @return array
     */
    public function hi()
    {
        return array(
            '#type' => 'marckup',
            '#marckup' => 'Welcom to Drupal 8'
        );
    }
}