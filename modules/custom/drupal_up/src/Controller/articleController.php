<?php

namespace Drupal\drupal_up\Controller;


/**
 * Class article_list
 * @package Drupal\drupal_up\Controller
 */
class articleController
{
    /**
     * @return array
     */
    public function articleList()
    {
        $items = array(
            array('name' => 'Drupal up article 1'),
            array('name' => 'Drupal up article 2'),
            array('name' => 'Drupal up article 3'),
            array('name' => 'Drupal up article 4'),
            array('name' => 'Drupal up article 5'),
        );
        return array(
            '#theme' => 'article_list',
            '#items' => $items,
            '#title' => 'Our article list'
        );
    }
}